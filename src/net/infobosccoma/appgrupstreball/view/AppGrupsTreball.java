package net.infobosccoma.appgrupstreball.view;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import java.util.Iterator;
import java.util.Scanner;
import net.infobosccoma.appgrupstreball.model.Estudiant;
import net.infobosccoma.appgrupstreball.model.GrupTreball;

/**
 * Classe que conté el mètode principal de l'aplicació de gestió d'estudiants i
 * grups de treball.
 *
 * L'aplicació ha d gestionar, mitjançant una BDOO db4o, a quins grups de
 * treball pertanyen diferents estudiants dins una escola. Donat un grup de
 * treball, aquest pot tenir assignats diversos estudiants, però tot estudiant
 * només té un (però sempre un) únic grup de treball. L’aplicació ha de poder
 * fer el següent: - Donar d’alta un nou estudiant. A l’hora d’assignar-li un
 * grup, si el nom indicat no existeix, es crea un nou grup. Si existeix, a
 * l’estudiant se li assigna aquell grup. - Reassignar un estudiant a un altre
 * grup de treball. Aquest grup ja ha d’existir. Si, en fer-ho, el grup antic
 * queda sense membres, cal esborrar-lo de la BDOO. - Llistar tots els grups
 * existents. - Llistar tots els estudiants (i a quin grup pertanyen).
 *
 * Es considera que els noms dels grups i dels estudiants són únics al sistema.
 * No hi pot haver noms repetits. En base a la descripció, també cal remarcar
 * que l’única manera de crear grups nous és afegint-hi nous estudiants.
 *
 * @author Joan Vilalta
 */
public class AppGrupsTreball {

    static Scanner teclat = new Scanner(System.in);

    public static void main(String[] args) {
        while (gMenu());
    }

    static boolean gMenu() {
        System.out.println("Escolleixi una de les seguents opcions:");
        System.out.println("\t 1) Mostar tots els estudiants");
        System.out.println("\t 2) Mostrar tots els grups");
        System.out.println("\t 3) Afegir un nou estudiant");
        System.out.println("\t 4) Reassignar un estudiant");
        System.out.println("\t 0) Sortir");

        switch (Byte.parseByte(teclat.nextLine())) {
            case 1:
                mostarEstudiants();
                break;
            case 2:
                mostrarGrups();
                break;
            case 3:
                addEstudiant();
                break;
            case 4:
                modEstudiant();
                break;
            case 0:
                return false;
            default:
                System.out.println("Opció incorrecte introdueixi un dels numeros del menu.");
                break;
        }
        return true;
    }

    private static void mostarEstudiants() {
        ObjectContainer db = Db4oEmbedded.openFile("laMevaBD.db4o");
        try {
            Estudiant estudiantQuery = new Estudiant(null, new GrupTreball(null, null));
            ObjectSet<Estudiant> result = db.queryByExample(estudiantQuery);
            mostrar(result);
        } finally {
            db.close();
        }
    }

    private static void mostrarGrups() {
        ObjectContainer db = Db4oEmbedded.openFile("laMevaBD.db4o");
        try {
            GrupTreball grupQuery = new GrupTreball(null, null);
            ObjectSet<GrupTreball> result = db.queryByExample(grupQuery);
            mostrar2(result);
        } finally {
            db.close();
        }
    }

    private static void addEstudiant() {
        ObjectContainer db = Db4oEmbedded.openFile("laMevaBD.db4o");
        boolean grupNou = false;
        GrupTreball grupObj = null;
        Estudiant estObj;
        try {
            System.out.println("Com es diu: ");
            String nom = teclat.nextLine();
            estObj = existeixEstudiant(db, nom);
            if (estObj == null) {
                System.out.println("A quin grup pertany: ");
                String grup = teclat.nextLine();
                grupObj = existeixGrup(db, grup);
                if (grupObj == null) {
                    System.out.println("Quin tema tracta el grup: ");
                    String tema = teclat.nextLine();
                    grupObj = new GrupTreball(grup, tema);
                    //grupNou = true;
                    estObj = new Estudiant(nom, grupObj);
                    db.store(estObj);
                } else {
                    estObj = new Estudiant(nom, grupObj);
                    db.store(estObj);
                }
            } else {
                System.out.printf("Ja existeix l'estudiant %s\n", nom);
            }

        } finally {
            db.close();
            //if(grupNou){storeGrup(grupObj);}
        }
    }

    private static void modEstudiant() {
        ObjectContainer db = Db4oEmbedded.openFile("laMevaBD.db4o");
        try {
            System.out.println("Com es diu l'Estudiant: ");
            String nom = teclat.nextLine();
            Estudiant estObj = existeixEstudiant(db, nom);
            if (estObj == null) {
                System.out.printf("No existeix l'estudiant %s\n", nom);
            } else {
                System.out.println("A quin grup els vols assignar: ");
                String grup = teclat.nextLine();
                GrupTreball grupObj = existeixGrup(db, grup);
                if (grupObj == null) {
                    System.out.printf("No existeix el grup %s\n", nom);
                } else {
                    db.delete(estObj.getGrupTreball());
                    estObj.reassignaGrup(grupObj);
                    db.store(estObj);
                }
            }

        } finally {
            db.close();
            //borrarGrup();
        }
    }

    static void mostrar(ObjectSet<Estudiant> result) {
        Iterator<Estudiant> it = result.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }

    static void mostrar2(ObjectSet<GrupTreball> result) {
        Iterator<GrupTreball> it = result.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }

    static GrupTreball existeixGrup(ObjectContainer db, String grup) {
        GrupTreball grupQuery = new GrupTreball(grup, null);
        ObjectSet<GrupTreball> result = db.queryByExample(grupQuery);
        return result.size() >= 1 ? result.next() : null;
    }

    static Estudiant existeixEstudiant(ObjectContainer db, String estudiant) {
        Estudiant grupQuery = new Estudiant(estudiant, new GrupTreball(null, null));
        ObjectSet<Estudiant> result = db.queryByExample(grupQuery);
        return result.size() >= 1 ? result.next() : null;
    }
    /*
    private static void borrarGrup() {
        ObjectContainer db = Db4oEmbedded.openFile("laMevaBD.db4o");
        try {
            GrupTreball grupQuery = new GrupTreball(null, null);
            ObjectSet<GrupTreball> result = db.queryByExample(grupQuery);
            for (GrupTreball grupTreball : result) {
                if (grupTreball.getNumEstudiants()<1) {
                    db.delete(grupTreball);
                }
            }            
        } finally {
            db.close();
        }
    }*/
 /*
    private static void storeGrup(GrupTreball grup){
        ObjectContainer db = Db4oEmbedded.openFile("laMevaBD.db4o");
        try {
            db.store(grup);            
        } finally {
            db.close();
        }
    }*/

}
